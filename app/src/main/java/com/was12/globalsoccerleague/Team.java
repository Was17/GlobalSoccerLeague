package com.was12.globalsoccerleague;

import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;



/**
 * Created by Jesus on 18/12/2017.
 */

public class Team {
    private String name;
    private String country;
    private int escudo;
    private int color;
    public Team() {
    }

    public Team(String name, String country, int escudo) {
        this.name = name;
        this.country = country;
        this.escudo = escudo;
    }


    public Team(String name, int escudo) {
        this.name = name;
        this.escudo = escudo;
        color= Color.CYAN;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getEscudo() {
        return escudo;
    }

    public void setEscudo(int escudo) {
        this.escudo = escudo;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
