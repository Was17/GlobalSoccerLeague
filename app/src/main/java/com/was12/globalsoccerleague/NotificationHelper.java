package com.was12.globalsoccerleague;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

/**
 * Created by Jesus on 16/12/2017.
 */

public class NotificationHelper extends ContextWrapper {
    public static final String channel1ID="channel1ID";
    public static final String channel1name="Channel 1";
    public static final String channel2ID="channel2ID";
    public static final String channel2name="Channel 2";
    private NotificationManager mManager;
    public NotificationHelper(Context base) {
        super(base);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            createChannels();}
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createChannels() {
           NotificationChannel channel1=new NotificationChannel(channel1ID,channel1name, NotificationManager.IMPORTANCE_DEFAULT);
            channel1.enableLights(true);
            channel1.enableVibration(true);
            channel1.setLightColor(R.color.champions);
            channel1.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            getManager().createNotificationChannel(channel1);
            NotificationChannel channel2=new NotificationChannel(channel2ID,channel2name, NotificationManager.IMPORTANCE_DEFAULT);
            channel2.enableLights(true);
            channel2.enableVibration(true);
            channel2.setLightColor(R.color.colorPrimary);
            channel2.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            getManager().createNotificationChannel(channel2);

    }
    public NotificationManager getManager(){
        if(mManager==null){
            mManager=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return mManager;
    }
    public NotificationCompat.Builder getChannel1Notification(String tittle,String message){
        return new NotificationCompat.Builder(getApplicationContext(),channel1ID)
                .setContentTitle(tittle)
                .setContentText(message)
                .setSmallIcon(R.drawable.cr7);

    }
    public NotificationCompat.Builder getChannel2Notification(String tittle,String message){
        return new NotificationCompat.Builder(getApplicationContext(),channel2ID)
                .setContentTitle(tittle)
                .setContentText(message)
                .setSmallIcon(R.drawable.cr7);

    }
}
