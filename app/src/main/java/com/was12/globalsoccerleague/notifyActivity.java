package com.was12.globalsoccerleague;

import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class notifyActivity extends AppCompatActivity {
private NotificationHelper notificationHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify);
        Button button= findViewById(R.id.button);
        notificationHelper=new NotificationHelper(this);
        Button button1=findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setChanel1("preba","dg");
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setChanel2("preba","dg");
            }
        });
    }

    private void setChanel1(String tittle,String message) {
        NotificationCompat.Builder nd=notificationHelper.getChannel1Notification(tittle,message);
    notificationHelper.getManager().notify(1,nd.build());
    }

    private void setChanel2(String tittle,String message) {
        NotificationCompat.Builder nd=notificationHelper.getChannel2Notification(tittle,message);

        notificationHelper.getManager().notify(2,nd.build());

    }
}
