package com.was12.globalsoccerleague;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayList<Team> objetoList;
        objetoList=new ArrayList<>();
        objetoList.add(new Team("FC Barcelona",R.drawable.cr7));
        objetoList.add(new Team("FC Barcelona",R.drawable.cr7));
        objetoList.add(new Team("FC Barcelona",R.drawable.cr7));
        objetoList.add(new Team("FC Barcelona",R.drawable.cr7));
        objetoList.add(new Team("FC Barcelona",R.drawable.cr7));
        objetoList.add(new Team("FC Barcelona",R.drawable.cr7));
        objetoList.add(new Team("FC Barcelona",R.drawable.cr7));
        objetoList.add(new Team("FC Barcelona",R.drawable.cr7));
        objetoList.add(new Team("FC Barcelona",R.drawable.cr7));
        objetoList.add(new Team("FC Barcelona",R.drawable.cr7));
        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.my_recycler_view);
        RecyclerView.LayoutManager manager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        RecyclerView.Adapter adapter=new TeamAdapter(this,objetoList);
        recyclerView.setAdapter(adapter);
    }
}
