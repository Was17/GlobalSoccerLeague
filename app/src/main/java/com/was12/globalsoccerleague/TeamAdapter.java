package com.was12.globalsoccerleague;

/**
 * Created by Jesus on 18/12/2017.
 */

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.TeamViewHolder> {

    ArrayList<Team> objetos;
    @Override
    public TeamViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_team,parent,false);
        return new TeamViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TeamViewHolder holder, int position) {
        Team objeto= objetos.get(position);
        holder.foto.setImageResource(objeto.getEscudo());
        holder.nombre.setText(objeto.getName());
        holder.mCardViewTop.setCardBackgroundColor(Color.GRAY);
    }

    public TeamAdapter(MainActivity mainActivity,ArrayList<Team> teams) {
        this.objetos = teams;

    }

    @Override
    public int getItemCount() {
        return objetos.size();
    }

    public class TeamViewHolder extends RecyclerView.ViewHolder {
        private ImageView foto;
        private TextView nombre;
        private CardView mCardViewTop;
        private View insideLayout;
        private TextView division;
        private  Context context;
        public TeamViewHolder(final View itemView) {
            super(itemView);
            mCardViewTop=itemView.findViewById(R.id.card_team);
           insideLayout = itemView.findViewById(R.id.layout_card_team);
            foto=(ImageView) itemView.findViewById(R.id.coverImageView);
            context=itemView.getContext();
            nombre=(TextView) itemView.findViewById(R.id.textcardNombreEquipo);

        }
    }
}